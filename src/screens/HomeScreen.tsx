import React from "react";
import {ActivityIndicator, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';

export function HomeScreen({ navigation }: any) {
    return (
        <View style={ styles.welcome }>
            <Text>Welcome Titouan</Text>
            <ActivityIndicator size="large" />
            <TouchableOpacity onPress={() => navigation.navigate('SecondHome')}>
                <Text>Go to SecondHomeStack</Text>
            </TouchableOpacity>
            <Icon.Button
                name="home"
                color="black"
            >
            </Icon.Button>
        </View>
    );
}
export function SecondHomeScreen() {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>SecondHomeScreen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    welcome: {
        flex: 1,
        width: 300,
    },
});
