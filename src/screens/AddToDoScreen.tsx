import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

export function AddToDoScreen({ navigation }: any) {
    return (
        <View style={ styles.welcome }>
            <Text>Ajout d'une Todolist</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    welcome: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
