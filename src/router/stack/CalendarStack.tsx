import React from "react";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {Button, Text, View} from "react-native";
import {CalendarScreen} from "../../screens/CalendarScreen.tsx";

const Stack = createNativeStackNavigator();
export function CalendarStack() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="About" component={CalendarScreen} />
        </Stack.Navigator>
    )
}