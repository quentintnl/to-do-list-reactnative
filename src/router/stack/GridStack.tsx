import React from "react";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {GridScreen} from "../../screens/GridScreen.tsx";

const Stack = createNativeStackNavigator();
export function GridStack() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Grille" component={GridScreen} />
        </Stack.Navigator>
    )
}