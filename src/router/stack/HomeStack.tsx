import React from "react";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {HomeScreen, SecondHomeScreen} from "../../screens/HomeScreen.tsx";

const Stack = createNativeStackNavigator();

export function HomeStack() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="SecondHome" component={SecondHomeScreen}/>
        </Stack.Navigator>
    )
}

