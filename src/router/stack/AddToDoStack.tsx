import React from "react";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import { AddToDoScreen } from "../../screens/AddToDoScreen.tsx";

const Stack = createNativeStackNavigator();
export function AddToDoStack() {
    return (
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="About" component={AddToDoScreen} />
        </Stack.Navigator>
    )
}