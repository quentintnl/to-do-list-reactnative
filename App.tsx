/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import * as React from 'react';
import type {PropsWithChildren} from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  StyleSheet,
  useColorScheme,
} from 'react-native';

import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { HomeStack } from "./src/router/stack/HomeStack.tsx";
import { CalendarStack } from "./src/router/stack/CalendarStack.tsx";
import {AddToDoStack} from "./src/router/stack/AddToDoStack.tsx";

import Icon from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {ProfileStack} from "./src/router/stack/ProfileStack.tsx";
import {GridStack} from "./src/router/stack/GridStack.tsx";

const Tab = createBottomTabNavigator();

function App(): React.JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  return (
      <SafeAreaProvider>
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={({ route }) => ({
                    headerShown: false,
                    tabBarIcon: ({ focused }) => {
                        let iconName;

                        if (route.name === 'Home') {
                            iconName = 'home'
                            return <Icon name={iconName} color="black" size={30} />;
                        } else if (route.name === 'Calendar') {
                            iconName = 'calendar'
                            return <Ionicons name={iconName} color="black" size={30} />;
                        } else if (route.name === 'AddToDo') {
                            iconName = 'add-circle-outline'
                            return <Ionicons name={iconName} color="black" size={30} />;
                        } else if (route.name === 'Profile') {
                            iconName = 'person-sharp'
                            return <Ionicons name={iconName} color="black" size={30} />;
                        }
                        else if (route.name === 'Grille') {
                            iconName = 'grid'
                            return <Ionicons name={iconName} color="black" size={30} />;
                        }
                    },
                })}
            >
                <Tab.Screen name="Home" component={HomeStack} />
                <Tab.Screen name="Calendar" component={CalendarStack} />
                <Tab.Screen name="AddToDo" component={AddToDoStack} />
                <Tab.Screen name="Profile" component={ProfileStack} />
                <Tab.Screen name="Grille" component={GridStack} />
            </Tab.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
  )
}


export default App;
